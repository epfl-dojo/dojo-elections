# Feature Request

This should describe a feature request. Please elaborate and give precise hint
on what you request and why you think this is a good feature to have. Links to
references and documentation about how to implement it are welcome.